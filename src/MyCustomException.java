public class MyCustomException extends Exception{
    //constructor with parameter
    MyCustomException(String message){
        super(message);
    }
}
