public class CustomList<T>{
    public int count;
    public int length;
    public Object arrList[];

    //Default constructor
    public CustomList(){
        arrList = new Object[1];
        count = 0;
        length = 1;
    }

    //method to add item to list
    public void setValue(T value) throws MyCustomException {
        if(value == null){
            throw new MyCustomException("Inputted value null");
        }
        for (int index=0; index<count; index++){
            if(arrList[index] == value){
                throw new MyCustomException("Duplicate Value : "+value);
            }
        }

        //increase size of array dynamically
        if(count == length){
            Object[] newArr = new Object[length *2];
            if(length >= 0) System.arraycopy(arrList,0,newArr,0,length);
            arrList = newArr;
            length = length*2;
        }
        arrList[count] = value;
        count++;

    }

    //method to get item from list
    public Object getValue(int position) {
        return arrList[position];
    }

    //method to get size of the list
    public int length(){
        return count;
    }
}
